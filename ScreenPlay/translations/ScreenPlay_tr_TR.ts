<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr_TR" sourcelanguage="en">
<context>
    <name>Community</name>
    <message>
        <source>News</source>
        <translation>Yenilikler</translation>
    </message>
    <message>
        <source>Wiki</source>
        <translation>Wiki</translation>
    </message>
    <message>
        <source>Forum</source>
        <translation>Forum</translation>
    </message>
    <message>
        <source>Contribute</source>
        <translation>Bağış yap</translation>
    </message>
    <message>
        <source>Steam Workshop</source>
        <translation>Steam Atölyesi</translation>
    </message>
    <message>
        <source>Issue Tracker</source>
        <translation type="unfinished">Issue Tracker</translation>
    </message>
    <message>
        <source>Reddit</source>
        <translation type="unfinished">Reddit</translation>
    </message>
</context>
<context>
    <name>CommunityNavItem</name>
    <message>
        <source>Open in browser</source>
        <translation>Tarayıcıda aç</translation>
    </message>
</context>
<context>
    <name>CreateSidebar</name>
    <message>
        <source>Tools Overview</source>
        <translation type="unfinished">Tools Overview</translation>
    </message>
    <message>
        <source>Video Import h264 (.mp4)</source>
        <translation type="unfinished">Video Import h264 (.mp4)</translation>
    </message>
    <message>
        <source>Video Import VP8 &amp; VP9 (.webm)</source>
        <translation type="unfinished">Video Import VP8 &amp; VP9 (.webm)</translation>
    </message>
    <message>
        <source>Video import (all types)</source>
        <translation type="unfinished">Video import (all types)</translation>
    </message>
    <message>
        <source>GIF Wallpaper</source>
        <translation type="unfinished">GIF Wallpaper</translation>
    </message>
    <message>
        <source>QML Wallpaper</source>
        <translation type="unfinished">QML Wallpaper</translation>
    </message>
    <message>
        <source>HTML5 Wallpaper</source>
        <translation type="unfinished">HTML5 Wallpaper</translation>
    </message>
    <message>
        <source>Website Wallpaper</source>
        <translation type="unfinished">Website Wallpaper</translation>
    </message>
    <message>
        <source>QML Widget</source>
        <translation type="unfinished">QML Widget</translation>
    </message>
    <message>
        <source>HTML Widget</source>
        <translation type="unfinished">HTML Widget</translation>
    </message>
</context>
<context>
    <name>CreateWallpaperInit</name>
    <message>
        <source>Import any video type</source>
        <translation>Video içe aktarın</translation>
    </message>
    <message>
        <source>Depending on your PC configuration it is better to convert your wallpaper to a specific video codec. If both have bad performance you can also try a QML wallpaper! Supported video formats are: 

*.mp4  *.mpg *.mp2 *.mpeg *.ogv *.avi *.wmv *.m4v *.3gp *.flv</source>
        <translation>PC yapılandırmanıza bağlı olarak, duvar kağıdınızı belirli bir video codec bileşenine dönüştürmek daha iyidir. Her ikisinin de performansı kötüyse, bir QML duvar kağıdını da deneyebilirsiniz! Desteklenen video biçimleri şunlardır:

*.mp4  *.mpg *.mp2 *.mpeg *.ogv *.avi *.wmv *.m4v *.3gp *.flv</translation>
    </message>
    <message>
        <source>Set your preffered video codec:</source>
        <translation>Tercih ettiğiniz video codec bileşenini ayarlayın:</translation>
    </message>
    <message>
        <source>Quality slider. Lower value means better quality.</source>
        <translation>Kalite kaydırıcısı. Daha düşük değer, daha iyi kalite anlamına gelir.</translation>
    </message>
    <message>
        <source>Open Documentation</source>
        <translation>Belgeyi Aç</translation>
    </message>
    <message>
        <source>Select file</source>
        <translation>Dosya seç</translation>
    </message>
</context>
<context>
    <name>CreateWallpaperResult</name>
    <message>
        <source>An error occurred!</source>
        <translation>Bir hata oluştu!</translation>
    </message>
    <message>
        <source>Copy text to clipboard</source>
        <translation>Metni panoya kopyala</translation>
    </message>
    <message>
        <source>Back to create and send an error report!</source>
        <translation>Bir hata raporu oluşturmak ve göndermek için geri dönün!</translation>
    </message>
</context>
<context>
    <name>CreateWallpaperVideoImportConvert</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Generating preview image...</source>
        <translation>Önizleme oluşturuluyor...</translation>
    </message>
    <message>
        <source>Generating preview thumbnail image...</source>
        <translation>Önizleme küçük resmi oluşturuluyor...</translation>
    </message>
    <message>
        <source>Generating 5 second preview video...</source>
        <translation>5 saniyelik önizleme videosu oluşturuluyor...</translation>
    </message>
    <message>
        <source>Generating preview gif...</source>
        <translation>Özizleme gifi oluşturuluyor...</translation>
    </message>
    <message>
        <source>Converting Audio...</source>
        <translation>Ses Dönüştürülüyor...</translation>
    </message>
    <message>
        <source>Converting Video... This can take some time!</source>
        <translation>Video dönüştürülüyor... Biraz zaman alabilir!</translation>
    </message>
    <message>
        <source>Converting Video ERROR!</source>
        <translation>Video Dönüştürülürken Hata Oluştu!</translation>
    </message>
    <message>
        <source>Analyse Video ERROR!</source>
        <translation>Video Hatasını Analiz Edin!</translation>
    </message>
    <message>
        <source>Convert a video to a wallpaper</source>
        <translation>Bir videoyu duvar kağıdına dönüştürün</translation>
    </message>
    <message>
        <source>Generating preview video...</source>
        <translation>Önizleme videosu oluşturuluyor...</translation>
    </message>
    <message>
        <source>Name (required!)</source>
        <translation>İsim (gerekli)</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Açıklama</translation>
    </message>
    <message>
        <source>Youtube URL</source>
        <translation>YouTube URL</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation>İptal</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Kaydet</translation>
    </message>
    <message>
        <source>Save Wallpaper...</source>
        <translation>Duvar kağıdını kaydet...</translation>
    </message>
</context>
<context>
    <name>DefaultVideoControls</name>
    <message>
        <source>Volume</source>
        <translation>Ses Düzeyi</translation>
    </message>
    <message>
        <source>Current Video Time</source>
        <translation>Geçerli Video Zamanı</translation>
    </message>
    <message>
        <source>Fill Mode</source>
        <translation>Doldurma Kipi</translation>
    </message>
    <message>
        <source>Stretch</source>
        <translation>Esnet</translation>
    </message>
    <message>
        <source>Fill</source>
        <translation>Doldur</translation>
    </message>
    <message>
        <source>Contain</source>
        <translation>İçeren</translation>
    </message>
    <message>
        <source>Cover</source>
        <translation>Kapak</translation>
    </message>
    <message>
        <source>Scale_Down</source>
        <translation>Ölçek_Düşür</translation>
    </message>
</context>
<context>
    <name>ExitPopup</name>
    <message>
        <source>Minimize ScreenPlay</source>
        <translation type="unfinished">Minimize ScreenPlay</translation>
    </message>
    <message>
        <source>Always minimize ScreenPlay</source>
        <translation type="unfinished">Always minimize ScreenPlay</translation>
    </message>
    <message>
        <source>You have active Wallpaper.
ScreenPlay will only quit if no Wallpaper are running.</source>
        <translation type="unfinished">You have active Wallpaper.
ScreenPlay will only quit if no Wallpaper are running.</translation>
    </message>
    <message>
        <source>You can &lt;b&gt;quit&lt;/b&gt; ScreenPlay via the bottom right Tray-Icon.</source>
        <translation type="unfinished">You can &lt;b&gt;quit&lt;/b&gt; ScreenPlay via the bottom right Tray-Icon.</translation>
    </message>
    <message>
        <source>You can &lt;b&gt;quit&lt;/b&gt; ScreenPlay via the top right Tray-Icon.</source>
        <translation type="unfinished">You can &lt;b&gt;quit&lt;/b&gt; ScreenPlay via the top right Tray-Icon.</translation>
    </message>
    <message>
        <source>Quit ScreenPlay now</source>
        <translation type="unfinished">Quit ScreenPlay now</translation>
    </message>
</context>
<context>
    <name>GifWallpaper</name>
    <message>
        <source>Import a Gif Wallpaper</source>
        <translation>Bir Gif Duvar Kağıdını İçe Aktarın</translation>
    </message>
    <message>
        <source>Drop a *.gif file here or use &apos;Select file&apos; below.</source>
        <translation>Bir *.gif dosyasını sürükleyin &apos; Ya da Dosyayı seçin &apos;</translation>
    </message>
    <message>
        <source>Select your gif</source>
        <translation>Gif seç</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Genel</translation>
    </message>
    <message>
        <source>Wallpaper name</source>
        <translation>Duvar Kağıdı adı</translation>
    </message>
    <message>
        <source>Created By</source>
        <translation>Oluşturan</translation>
    </message>
    <message>
        <source>Tags</source>
        <translation>Etiketler</translation>
    </message>
</context>
<context>
    <name>HTMLWallpaper</name>
    <message>
        <source>Create a HTML Wallpaper</source>
        <translation>HTML Duvar Kağıdı Oluşturun</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Genel</translation>
    </message>
    <message>
        <source>Wallpaper name</source>
        <translation>Duvar Kağıdı adı</translation>
    </message>
    <message>
        <source>Created By</source>
        <translation>Oluşturan</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Açıklama</translation>
    </message>
    <message>
        <source>License &amp; Tags</source>
        <translation>Lisans &amp; Etiketler</translation>
    </message>
    <message>
        <source>Preview Image</source>
        <translation>Resim Önizleme</translation>
    </message>
</context>
<context>
    <name>HTMLWidget</name>
    <message>
        <source>Create a HTML widget</source>
        <translation>Bir HTML widget&apos;ı oluşturun</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Genel</translation>
    </message>
    <message>
        <source>Widget name</source>
        <translation>Widget adı</translation>
    </message>
    <message>
        <source>Created by</source>
        <translation>Oluşturan</translation>
    </message>
    <message>
        <source>Tags</source>
        <translation>Etiketler</translation>
    </message>
</context>
<context>
    <name>ImportWebmConvert</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>AnalyseVideo...</source>
        <translation>VideoAnaliz...</translation>
    </message>
    <message>
        <source>Generating preview image...</source>
        <translation>Önizleme oluşturuluyor...</translation>
    </message>
    <message>
        <source>Generating preview thumbnail image...</source>
        <translation>Önizleme küçük resmi oluşturuluyor...</translation>
    </message>
    <message>
        <source>Generating 5 second preview video...</source>
        <translation>5 saniyelik önizleme videosu oluşturuluyor...</translation>
    </message>
    <message>
        <source>Generating preview gif...</source>
        <translation>Özizleme gifi oluşturuluyor...</translation>
    </message>
    <message>
        <source>Converting Audio...</source>
        <translation>Ses Dönüştürülüyor...</translation>
    </message>
    <message>
        <source>Converting Video... This can take some time!</source>
        <translation>Video dönüştürülüyor... Biraz zaman alabilir!</translation>
    </message>
    <message>
        <source>Converting Video ERROR!</source>
        <translation>Video Dönüştürülürken Hata Oluştu!</translation>
    </message>
    <message>
        <source>Analyse Video ERROR!</source>
        <translation>Video Hatasını Analiz Edin!</translation>
    </message>
    <message>
        <source>Import a video to a wallpaper</source>
        <translation>Bir videoyu duvar kağıdına aktarın</translation>
    </message>
    <message>
        <source>Generating preview video...</source>
        <translation>Önizleme videosu oluşturuluyor...</translation>
    </message>
    <message>
        <source>Name (required!)</source>
        <translation>İsim (gerekli)</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Açıklama</translation>
    </message>
    <message>
        <source>Youtube URL</source>
        <translation>YouTube URL</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation>İptal</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Kaydet</translation>
    </message>
    <message>
        <source>Save Wallpaper...</source>
        <translation>Duvar kağıdını kaydet...</translation>
    </message>
</context>
<context>
    <name>ImportWebmInit</name>
    <message>
        <source>Import a .webm video</source>
        <translation>.webm uzantılı bir videoyu içe aktarın</translation>
    </message>
    <message>
        <source>When importing webm we can skip the long conversion. When you get unsatisfying results with the ScreenPlay importer from &apos;ideo import and convert (all types)&apos; you can also convert via the free and open source HandBrake!</source>
        <translation>Webm&apos;yi içe aktarırken uzun dönüşümü atlayabiliriz. &apos; Video içe aktarma ve dönüştürmeden (tüm türler) ScreenPlay içe aktarıcı ile tatmin edici olmayan sonuçlar aldığınızda, ücretsiz ve açık kaynaklı El Freni aracılığıyla da dönüştürebilirsiniz! &apos;</translation>
    </message>
    <message>
        <source>Invalid file type. Must be valid VP8 or VP9 (*.webm)!</source>
        <translation>Geçersiz dosya türü. Geçerli VP8 veya VP9 (*.webm) olmalıdır!</translation>
    </message>
    <message>
        <source>Drop a *.webm file here or use &apos;Select file&apos; below.</source>
        <translation>Bir *.webm dosyasını sürükleyin &apos; Ya da Dosyayı seçin &apos;</translation>
    </message>
    <message>
        <source>Open Documentation</source>
        <translation>Belgeyi Aç</translation>
    </message>
    <message>
        <source>Select file</source>
        <translation>Dosya seç</translation>
    </message>
</context>
<context>
    <name>Importh264Convert</name>
    <message>
        <source>AnalyseVideo...</source>
        <translation type="unfinished">AnalyseVideo...</translation>
    </message>
    <message>
        <source>Generating preview image...</source>
        <translation type="unfinished">Generating preview image...</translation>
    </message>
    <message>
        <source>Generating preview thumbnail image...</source>
        <translation type="unfinished">Generating preview thumbnail image...</translation>
    </message>
    <message>
        <source>Generating 5 second preview video...</source>
        <translation type="unfinished">Generating 5 second preview video...</translation>
    </message>
    <message>
        <source>Generating preview gif...</source>
        <translation type="unfinished">Generating preview gif...</translation>
    </message>
    <message>
        <source>Converting Audio...</source>
        <translation type="unfinished">Converting Audio...</translation>
    </message>
    <message>
        <source>Converting Video... This can take some time!</source>
        <translation type="unfinished">Converting Video... This can take some time!</translation>
    </message>
    <message>
        <source>Converting Video ERROR!</source>
        <translation type="unfinished">Converting Video ERROR!</translation>
    </message>
    <message>
        <source>Analyse Video ERROR!</source>
        <translation type="unfinished">Analyse Video ERROR!</translation>
    </message>
    <message>
        <source>Import a video to a wallpaper</source>
        <translation type="unfinished">Import a video to a wallpaper</translation>
    </message>
    <message>
        <source>Generating preview video...</source>
        <translation type="unfinished">Generating preview video...</translation>
    </message>
    <message>
        <source>Name (required!)</source>
        <translation type="unfinished">Name (required!)</translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="unfinished">Description</translation>
    </message>
    <message>
        <source>Youtube URL</source>
        <translation type="unfinished">Youtube URL</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation type="unfinished">Abort</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="unfinished">Save</translation>
    </message>
    <message>
        <source>Save Wallpaper...</source>
        <translation type="unfinished">Save Wallpaper...</translation>
    </message>
</context>
<context>
    <name>Importh264Init</name>
    <message>
        <source>Import a .mp4 video</source>
        <translation type="unfinished">Import a .mp4 video</translation>
    </message>
    <message>
        <source>ScreenPlay V0.15 and up can play *.mp4 (also more known as h264). This can improove performance on older systems.</source>
        <translation type="unfinished">ScreenPlay V0.15 and up can play *.mp4 (also more known as h264). This can improove performance on older systems.</translation>
    </message>
    <message>
        <source>Invalid file type. Must be valid h264 (*.mp4)!</source>
        <translation type="unfinished">Invalid file type. Must be valid h264 (*.mp4)!</translation>
    </message>
    <message>
        <source>Drop a *.mp4 file here or use &apos;Select file&apos; below.</source>
        <translation type="unfinished">Drop a *.mp4 file here or use &apos;Select file&apos; below.</translation>
    </message>
    <message>
        <source>Open Documentation</source>
        <translation type="unfinished">Open Documentation</translation>
    </message>
    <message>
        <source>Select file</source>
        <translation type="unfinished">Select file</translation>
    </message>
</context>
<context>
    <name>Installed</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Refreshing!</source>
        <translation>Yenileniyor!</translation>
    </message>
    <message>
        <source>Pull to refresh!</source>
        <translation>Yenilemek için çek!</translation>
    </message>
    <message>
        <source>Get more Wallpaper &amp; Widgets via the Steam workshop!</source>
        <translation>Steam atölyesi aracılığıyla daha fazla Duvar Kağıdı ve Widget edinin!</translation>
    </message>
    <message>
        <source>Open containing folder</source>
        <translation>İçeren klasörü aç</translation>
    </message>
    <message>
        <source>Remove Item</source>
        <translation>Öğeyi Kaldır</translation>
    </message>
    <message>
        <source>Remove via Workshop</source>
        <translation>Atölye ile Kaldır</translation>
    </message>
    <message>
        <source>Open Workshop Page</source>
        <translation>Atölyeyi Aç</translation>
    </message>
    <message>
        <source>Are you sure you want to delete this item?</source>
        <translation>Bu öğeyi silmek istediğinizden emin misiniz?</translation>
    </message>
    <message>
        <source>Export</source>
        <translation type="unfinished">Export</translation>
    </message>
    <message>
        <source>We only support adding one item at once.</source>
        <translation type="unfinished">We only support adding one item at once.</translation>
    </message>
    <message>
        <source>File type not supported. We only support &apos;.screenplay&apos; files.</source>
        <translation type="unfinished">File type not supported. We only support &apos;.screenplay&apos; files.</translation>
    </message>
    <message>
        <source>Import Content...</source>
        <translation type="unfinished">Import Content...</translation>
    </message>
    <message>
        <source>Export Content...</source>
        <translation type="unfinished">Export Content...</translation>
    </message>
</context>
<context>
    <name>InstalledNavigation</name>
    <message>
        <source>All</source>
        <translation type="unfinished">All</translation>
    </message>
    <message>
        <source>Scenes</source>
        <translation type="unfinished">Scenes</translation>
    </message>
    <message>
        <source>Videos</source>
        <translation type="unfinished">Videos</translation>
    </message>
    <message>
        <source>Widgets</source>
        <translation type="unfinished">Widgets</translation>
    </message>
</context>
<context>
    <name>InstalledWelcomeScreen</name>
    <message>
        <source>Get free Widgets and Wallpaper via the Steam Workshop</source>
        <translation>Steam Atölyesi aracılığıyla ücretsiz Widget&apos;lar ve Duvar Kağıdı alın</translation>
    </message>
    <message>
        <source>Browse the Steam Workshop</source>
        <translation>Steam Atölyesine Göz Atın</translation>
    </message>
    <message>
        <source>Get content via our forum</source>
        <translation type="unfinished">Get content via our forum</translation>
    </message>
    <message>
        <source>Open the ScreenPlay forum</source>
        <translation type="unfinished">Open the ScreenPlay forum</translation>
    </message>
</context>
<context>
    <name>Monitors</name>
    <message>
        <source>Wallpaper Configuration</source>
        <translation>Duvar Kağıdı Yapılandırması</translation>
    </message>
    <message>
        <source>Remove selected</source>
        <translation>Seçilenleri kaldır</translation>
    </message>
    <message>
        <source>Wallpapers</source>
        <translation>Duvar kağıtları</translation>
    </message>
    <message>
        <source>Widgets</source>
        <translation>Widget’lar</translation>
    </message>
    <message>
        <source>Remove all </source>
        <translation type="unfinished">Remove all </translation>
    </message>
</context>
<context>
    <name>MonitorsProjectSettingItem</name>
    <message>
        <source>Set color</source>
        <translation>Renk seç</translation>
    </message>
    <message>
        <source>Please choose a color</source>
        <translation>Lütfen renk seçin</translation>
    </message>
</context>
<context>
    <name>Navigation</name>
    <message>
        <source>Create</source>
        <translation type="unfinished">Create</translation>
    </message>
    <message>
        <source>Workshop</source>
        <translation type="unfinished">Workshop</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation type="unfinished">Installed</translation>
    </message>
    <message>
        <source>Community</source>
        <translation type="unfinished">Community</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="unfinished">Settings</translation>
    </message>
    <message>
        <source>Mute/Unmute all Wallpaper</source>
        <translation type="unfinished">Mute/Unmute all Wallpaper</translation>
    </message>
    <message>
        <source>Pause/Play all Wallpaper</source>
        <translation type="unfinished">Pause/Play all Wallpaper</translation>
    </message>
    <message>
        <source>Configure Wallpaper</source>
        <translation type="unfinished">Configure Wallpaper</translation>
    </message>
    <message>
        <source>Support me on Patreon!</source>
        <translation type="unfinished">Support me on Patreon!</translation>
    </message>
    <message>
        <source>Close All Content</source>
        <translation type="unfinished">Close All Content</translation>
    </message>
</context>
<context>
    <name>QMLWallpaper</name>
    <message>
        <source>Create a QML Wallpaper</source>
        <translation>Bir QML Duvar Kağıdı Oluşturun</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Genel</translation>
    </message>
    <message>
        <source>Wallpaper name</source>
        <translation>Duvar Kağıdı adı</translation>
    </message>
    <message>
        <source>Created By</source>
        <translation>Oluşturan</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Açıklama</translation>
    </message>
    <message>
        <source>License &amp; Tags</source>
        <translation>Lisans &amp; Etiketler</translation>
    </message>
    <message>
        <source>Preview Image</source>
        <translation>Resim Önizleme</translation>
    </message>
</context>
<context>
    <name>QMLWallpaperMain</name>
    <message>
        <source>My ScreenPlay Wallpaper 🚀</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QMLWidget</name>
    <message>
        <source>Create a QML widget</source>
        <translation>Bir QML widget&apos;ı oluşturun</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Genel</translation>
    </message>
    <message>
        <source>Widget name</source>
        <translation>Widget adı</translation>
    </message>
    <message>
        <source>Created by</source>
        <translation>Oluşturan</translation>
    </message>
    <message>
        <source>Tags</source>
        <translation>Etiketler</translation>
    </message>
</context>
<context>
    <name>QMLWidgetMain</name>
    <message>
        <source>My Widget 🚀</source>
        <translation type="unfinished">My Widget 🚀</translation>
    </message>
</context>
<context>
    <name>SaveNotification</name>
    <message>
        <source>Profile saved successfully!</source>
        <translation>Profil başarıyla kaydedildi!</translation>
    </message>
</context>
<context>
    <name>ScreenPlayItem</name>
    <message>
        <source>NEW</source>
        <translation>YENİ</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <source>General</source>
        <translation>Genel</translation>
    </message>
    <message>
        <source>Autostart</source>
        <translation>Otomatik başlatma</translation>
    </message>
    <message>
        <source>ScreenPlay will start with Windows and will setup your Desktop every time for you.</source>
        <translation>ScreenPlay, Windows ile başlayacak ve her seferinde sizin için Masaüstünüzü kuracaktır.</translation>
    </message>
    <message>
        <source>High priority Autostart</source>
        <translation>Yüksek öncelikli Otomatik başlatma</translation>
    </message>
    <message>
        <source>This options grants ScreenPlay a higher autostart priority than other apps.</source>
        <translation>Bu seçenekler, ScreenPlay&apos;e diğer uygulamalardan daha yüksek bir otomatik başlatma önceliği verir.</translation>
    </message>
    <message>
        <source>Send anonymous crash reports and statistics</source>
        <translation type="unfinished">Send anonymous crash reports and statistics</translation>
    </message>
    <message>
        <source>Help us make ScreenPlay faster and more stable. All collected data is purely anonymous and only used for development purposes! We use &lt;a href=&quot;https://sentry.io&quot;&gt;sentry.io&lt;/a&gt; to collect and analyze this data. A &lt;b&gt;big thanks to them&lt;/b&gt; for providing us with free premium support for open source projects!</source>
        <translation type="unfinished">Help us make ScreenPlay faster and more stable. All collected data is purely anonymous and only used for development purposes! We use &lt;a href=&quot;https://sentry.io&quot;&gt;sentry.io&lt;/a&gt; to collect and analyze this data. A &lt;b&gt;big thanks to them&lt;/b&gt; for providing us with free premium support for open source projects!</translation>
    </message>
    <message>
        <source>Set save location</source>
        <translation type="unfinished">Set save location</translation>
    </message>
    <message>
        <source>Set location</source>
        <translation type="unfinished">Set location</translation>
    </message>
    <message>
        <source>Your storage path is empty!</source>
        <translation type="unfinished">Your storage path is empty!</translation>
    </message>
    <message>
        <source>Important: Changing this directory has no effect on the workshop download path. ScreenPlay only supports having one content folder!</source>
        <translation>Önemli: Bu dizini değiştirmenin atölye indirme yolu üzerinde hiçbir etkisi yoktur. ScreenPlay yalnızca bir içerik klasörüne sahip olmayı destekler!</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Dil</translation>
    </message>
    <message>
        <source>Set the ScreenPlay UI Language</source>
        <translation>ScreenPlay Arayüz Dilini Ayarlayın</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Switch dark/light theme</source>
        <translation>Koyu/Açık temayı değiştir</translation>
    </message>
    <message>
        <source>System Default</source>
        <translation>Sistem Varsayılanı</translation>
    </message>
    <message>
        <source>Dark</source>
        <translation>Karanlık</translation>
    </message>
    <message>
        <source>Light</source>
        <translation>Aydınlık</translation>
    </message>
    <message>
        <source>Performance</source>
        <translation>Performans</translation>
    </message>
    <message>
        <source>Pause wallpaper video rendering while another app is in the foreground</source>
        <translation>Başka bir uygulama ön plandayken duvar kağıdı video oluşturmayı duraklatın</translation>
    </message>
    <message>
        <source>We disable the video rendering (not the audio!) for the best performance. If you have problem you can disable this behaviour here. Wallpaper restart required!</source>
        <translation>En iyi performans için video oluşturmayı (sesi değil!) devre dışı bırakıyoruz. Sorun yaşıyorsanız, bu davranışı buradan devre dışı bırakabilirsiniz. Duvar kağıdının yeniden başlatılması gerekiyor!</translation>
    </message>
    <message>
        <source>Default Fill Mode</source>
        <translation>Varsayılan Doldurma Modu</translation>
    </message>
    <message>
        <source>Set this property to define how the video is scaled to fit the target area.</source>
        <translation>Videonun hedef alana sığacak şekilde nasıl ölçeklendirileceğini tanımlamak için bu özelliği ayarlayın.</translation>
    </message>
    <message>
        <source>Stretch</source>
        <translation>Esnet</translation>
    </message>
    <message>
        <source>Fill</source>
        <translation>Doldur</translation>
    </message>
    <message>
        <source>Contain</source>
        <translation>İçeren</translation>
    </message>
    <message>
        <source>Cover</source>
        <translation>Kapak</translation>
    </message>
    <message>
        <source>Scale-Down</source>
        <translation>Ölçek-Düşür</translation>
    </message>
    <message>
        <source>About</source>
        <translation>Hakkında</translation>
    </message>
    <message>
        <source>Thank you for using ScreenPlay</source>
        <translation>ScreePlay&apos;i kullandığınız için teşekkür ederiz!</translation>
    </message>
    <message>
        <source>Hi, I&apos;m Elias Steurer also known as Kelteseth and I&apos;m the developer of ScreenPlay. Thank you for using my software. You can follow me to receive updates about ScreenPlay here:</source>
        <translation type="unfinished">Hi, I&apos;m Elias Steurer also known as Kelteseth and I&apos;m the developer of ScreenPlay. Thank you for using my software. You can follow me to receive updates about ScreenPlay here:</translation>
    </message>
    <message>
        <source>Version</source>
        <translation type="unfinished">Version</translation>
    </message>
    <message>
        <source>Open Changelog</source>
        <translation type="unfinished">Open Changelog</translation>
    </message>
    <message>
        <source>Third Party Software</source>
        <translation type="unfinished">Third Party Software</translation>
    </message>
    <message>
        <source>ScreenPlay would not be possible without the work of others. A big thank you to: </source>
        <translation type="unfinished">ScreenPlay would not be possible without the work of others. A big thank you to: </translation>
    </message>
    <message>
        <source>Licenses</source>
        <translation type="unfinished">Licenses</translation>
    </message>
    <message>
        <source>Logs</source>
        <translation type="unfinished">Logs</translation>
    </message>
    <message>
        <source>If your ScreenPlay missbehaves this is a good way to look for answers. This shows all logs and warning during runtime.</source>
        <translation type="unfinished">If your ScreenPlay missbehaves this is a good way to look for answers. This shows all logs and warning during runtime.</translation>
    </message>
    <message>
        <source>Show Logs</source>
        <translation type="unfinished">Show Logs</translation>
    </message>
    <message>
        <source>Data Protection</source>
        <translation type="unfinished">Data Protection</translation>
    </message>
    <message>
        <source>We use you data very carefully to improve ScreenPlay. We do not sell or share this (anonymous) information with others!</source>
        <translation type="unfinished">We use you data very carefully to improve ScreenPlay. We do not sell or share this (anonymous) information with others!</translation>
    </message>
    <message>
        <source>Privacy</source>
        <translation type="unfinished">Privacy</translation>
    </message>
</context>
<context>
    <name>SettingsExpander</name>
    <message>
        <source>Copy text to clipboard</source>
        <translation type="unfinished">Copy text to clipboard</translation>
    </message>
</context>
<context>
    <name>Sidebar</name>
    <message>
        <source>Set Wallpaper</source>
        <translation type="unfinished">Set Wallpaper</translation>
    </message>
    <message>
        <source>Set Widget</source>
        <translation type="unfinished">Set Widget</translation>
    </message>
    <message>
        <source>Headline</source>
        <translation type="unfinished">Headline</translation>
    </message>
    <message>
        <source>Select a Monitor to display the content</source>
        <translation type="unfinished">Select a Monitor to display the content</translation>
    </message>
    <message>
        <source>Set Volume</source>
        <translation type="unfinished">Set Volume</translation>
    </message>
    <message>
        <source>Fill Mode</source>
        <translation type="unfinished">Fill Mode</translation>
    </message>
    <message>
        <source>Stretch</source>
        <translation type="unfinished">Stretch</translation>
    </message>
    <message>
        <source>Fill</source>
        <translation type="unfinished">Fill</translation>
    </message>
    <message>
        <source>Contain</source>
        <translation type="unfinished">Contain</translation>
    </message>
    <message>
        <source>Cover</source>
        <translation type="unfinished">Cover</translation>
    </message>
    <message>
        <source>Scale-Down</source>
        <translation type="unfinished">Scale-Down</translation>
    </message>
</context>
<context>
    <name>StartInfo</name>
    <message>
        <source>Free tools to help you to create wallpaper</source>
        <translation type="unfinished">Free tools to help you to create wallpaper</translation>
    </message>
    <message>
        <source>Below you can find tools to create wallaper, beyond the tools that ScreenPlay provides for you!</source>
        <translation type="unfinished">Below you can find tools to create wallaper, beyond the tools that ScreenPlay provides for you!</translation>
    </message>
</context>
<context>
    <name>WebsiteWallpaper</name>
    <message>
        <source>Create a Website Wallpaper</source>
        <translation type="unfinished">Create a Website Wallpaper</translation>
    </message>
    <message>
        <source>General</source>
        <translation type="unfinished">General</translation>
    </message>
    <message>
        <source>Wallpaper name</source>
        <translation type="unfinished">Wallpaper name</translation>
    </message>
    <message>
        <source>Created By</source>
        <translation type="unfinished">Created By</translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="unfinished">Description</translation>
    </message>
    <message>
        <source>Tags</source>
        <translation type="unfinished">Tags</translation>
    </message>
    <message>
        <source>Preview Image</source>
        <translation type="unfinished">Preview Image</translation>
    </message>
</context>
<context>
    <name>WizardPage</name>
    <message>
        <source>Save</source>
        <translation type="unfinished">Save</translation>
    </message>
    <message>
        <source>Saving...</source>
        <translation type="unfinished">Saving...</translation>
    </message>
</context>
<context>
    <name>XMLNewsfeed</name>
    <message>
        <source>News &amp; Patchnotes</source>
        <translation type="unfinished">News &amp; Patchnotes</translation>
    </message>
</context>
</TS>
