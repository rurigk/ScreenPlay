<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR" sourcelanguage="en">
<context>
    <name>Community</name>
    <message>
        <source>News</source>
        <translation>Actualités</translation>
    </message>
    <message>
        <source>Wiki</source>
        <translation>Wiki</translation>
    </message>
    <message>
        <source>Forum</source>
        <translation>Forum</translation>
    </message>
    <message>
        <source>Contribute</source>
        <translation>Contribuer</translation>
    </message>
    <message>
        <source>Steam Workshop</source>
        <translation>Steam Workshop</translation>
    </message>
    <message>
        <source>Issue Tracker</source>
        <translation>Issue Tracker</translation>
    </message>
    <message>
        <source>Reddit</source>
        <translation>Reddit</translation>
    </message>
</context>
<context>
    <name>CommunityNavItem</name>
    <message>
        <source>Open in browser</source>
        <translation>Ouvrir dans le navigateur</translation>
    </message>
</context>
<context>
    <name>CreateSidebar</name>
    <message>
        <source>Tools Overview</source>
        <translation>Aperçu des outils</translation>
    </message>
    <message>
        <source>Video Import h264 (.mp4)</source>
        <translation type="unfinished">Video Import h264 (.mp4)</translation>
    </message>
    <message>
        <source>Video Import VP8 &amp; VP9 (.webm)</source>
        <translation type="unfinished">Video Import VP8 &amp; VP9 (.webm)</translation>
    </message>
    <message>
        <source>Video import (all types)</source>
        <translation type="unfinished">Video import (all types)</translation>
    </message>
    <message>
        <source>GIF Wallpaper</source>
        <translation>Fond d&apos;écran GIF</translation>
    </message>
    <message>
        <source>QML Wallpaper</source>
        <translation>Fond d&apos;écran QML</translation>
    </message>
    <message>
        <source>HTML5 Wallpaper</source>
        <translation>Fond d&apos;écran HTML5</translation>
    </message>
    <message>
        <source>Website Wallpaper</source>
        <translation>Fond d&apos;écran web</translation>
    </message>
    <message>
        <source>QML Widget</source>
        <translation>Widget QML</translation>
    </message>
    <message>
        <source>HTML Widget</source>
        <translation>Widget HTML</translation>
    </message>
</context>
<context>
    <name>CreateWallpaperInit</name>
    <message>
        <source>Import any video type</source>
        <translation>Importer n’importe quel type de vidéo</translation>
    </message>
    <message>
        <source>Depending on your PC configuration it is better to convert your wallpaper to a specific video codec. If both have bad performance you can also try a QML wallpaper! Supported video formats are: 

*.mp4  *.mpg *.mp2 *.mpeg *.ogv *.avi *.wmv *.m4v *.3gp *.flv</source>
        <translation>Selon la configuration de votre PC, il est préférable de convertir votre fond d&apos;écran vers un codec vidéo spécifique. Si les deux ont de mauvaises performances, vous pouvez aussi essayer un fond d&apos;écran QML! Les formats vidéo pris en charge sont : 

*.mp4 *.mpg *.mp2 *.mpeg *.ogv *.avi *.wmv *.m4v *.3gp *.flv</translation>
    </message>
    <message>
        <source>Set your preffered video codec:</source>
        <translation>Définissez votre codec vidéo préféré :</translation>
    </message>
    <message>
        <source>Quality slider. Lower value means better quality.</source>
        <translation>Curseur de qualité. Une valeur plus faible signifie une meilleure qualité.</translation>
    </message>
    <message>
        <source>Open Documentation</source>
        <translation>Ouvrir la documentation</translation>
    </message>
    <message>
        <source>Select file</source>
        <translation>Sélectionner un fichier</translation>
    </message>
</context>
<context>
    <name>CreateWallpaperResult</name>
    <message>
        <source>An error occurred!</source>
        <translation>Une erreur est survenue !</translation>
    </message>
    <message>
        <source>Copy text to clipboard</source>
        <translation>Copier le texte dans le presse-papiers</translation>
    </message>
    <message>
        <source>Back to create and send an error report!</source>
        <translation>Retour à la création et envoyer un rapport d’erreur !</translation>
    </message>
</context>
<context>
    <name>CreateWallpaperVideoImportConvert</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Generating preview image...</source>
        <translation>Génération de l&apos;image d&apos;aperçu...</translation>
    </message>
    <message>
        <source>Generating preview thumbnail image...</source>
        <translation>Génération de la miniature de l’aperçu...</translation>
    </message>
    <message>
        <source>Generating 5 second preview video...</source>
        <translation>Génération d’une vidéo d’aperçu de 5 secondes...</translation>
    </message>
    <message>
        <source>Generating preview gif...</source>
        <translation>Génération du gif d&apos;aperçu...</translation>
    </message>
    <message>
        <source>Converting Audio...</source>
        <translation>Conversion de l’audio...</translation>
    </message>
    <message>
        <source>Converting Video... This can take some time!</source>
        <translation>Conversion de la vidéo... Cela peut prendre un certain temps !</translation>
    </message>
    <message>
        <source>Converting Video ERROR!</source>
        <translation>Erreur de conversion vidéo !</translation>
    </message>
    <message>
        <source>Analyse Video ERROR!</source>
        <translation>Erreur d&apos;Analyse Vidéo !</translation>
    </message>
    <message>
        <source>Convert a video to a wallpaper</source>
        <translation>Convertir une vidéo en fond d&apos;écran</translation>
    </message>
    <message>
        <source>Generating preview video...</source>
        <translation>Génération de la vidéo d&apos;aperçu...</translation>
    </message>
    <message>
        <source>Name (required!)</source>
        <translation>Nom (requis!)</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <source>Youtube URL</source>
        <translation>URL Youtube&#xa0;</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <source>Save Wallpaper...</source>
        <translation>Enregistrer le fond d&apos;écran</translation>
    </message>
</context>
<context>
    <name>DefaultVideoControls</name>
    <message>
        <source>Volume</source>
        <translation>Volume</translation>
    </message>
    <message>
        <source>Current Video Time</source>
        <translation>Progression de la vidéo en cours</translation>
    </message>
    <message>
        <source>Fill Mode</source>
        <translation>Mode d&apos;affichage</translation>
    </message>
    <message>
        <source>Stretch</source>
        <translation>Étirer</translation>
    </message>
    <message>
        <source>Fill</source>
        <translation>Remplir</translation>
    </message>
    <message>
        <source>Contain</source>
        <translation>Contenir</translation>
    </message>
    <message>
        <source>Cover</source>
        <translation>Couvrir</translation>
    </message>
    <message>
        <source>Scale_Down</source>
        <translation>Réduire</translation>
    </message>
</context>
<context>
    <name>ExitPopup</name>
    <message>
        <source>Minimize ScreenPlay</source>
        <translation type="unfinished">Minimize ScreenPlay</translation>
    </message>
    <message>
        <source>Always minimize ScreenPlay</source>
        <translation type="unfinished">Always minimize ScreenPlay</translation>
    </message>
    <message>
        <source>You have active Wallpaper.
ScreenPlay will only quit if no Wallpaper are running.</source>
        <translation type="unfinished">You have active Wallpaper.
ScreenPlay will only quit if no Wallpaper are running.</translation>
    </message>
    <message>
        <source>You can &lt;b&gt;quit&lt;/b&gt; ScreenPlay via the bottom right Tray-Icon.</source>
        <translation type="unfinished">You can &lt;b&gt;quit&lt;/b&gt; ScreenPlay via the bottom right Tray-Icon.</translation>
    </message>
    <message>
        <source>You can &lt;b&gt;quit&lt;/b&gt; ScreenPlay via the top right Tray-Icon.</source>
        <translation type="unfinished">You can &lt;b&gt;quit&lt;/b&gt; ScreenPlay via the top right Tray-Icon.</translation>
    </message>
    <message>
        <source>Quit ScreenPlay now</source>
        <translation type="unfinished">Quit ScreenPlay now</translation>
    </message>
</context>
<context>
    <name>GifWallpaper</name>
    <message>
        <source>Import a Gif Wallpaper</source>
        <translation>Importer un fond d&apos;écran Gif</translation>
    </message>
    <message>
        <source>Drop a *.gif file here or use &apos;Select file&apos; below.</source>
        <translation>Déposez un fichier *.gif ici ou utilisez &apos;Sélectionner un fichier&apos; ci-dessous.</translation>
    </message>
    <message>
        <source>Select your gif</source>
        <translation>Sélectionnez votre gif</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <source>Wallpaper name</source>
        <translation>Nom du fond d&apos;écran</translation>
    </message>
    <message>
        <source>Created By</source>
        <translation>Créé par</translation>
    </message>
    <message>
        <source>Tags</source>
        <translation>Tags</translation>
    </message>
</context>
<context>
    <name>HTMLWallpaper</name>
    <message>
        <source>Create a HTML Wallpaper</source>
        <translation>Créer un fond d&apos;écran HTML</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <source>Wallpaper name</source>
        <translation>Nom du fond d&apos;écran</translation>
    </message>
    <message>
        <source>Created By</source>
        <translation>Créé par</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <source>License &amp; Tags</source>
        <translation>Licence &amp; Tags</translation>
    </message>
    <message>
        <source>Preview Image</source>
        <translation>Image d&apos;aperçu</translation>
    </message>
</context>
<context>
    <name>HTMLWidget</name>
    <message>
        <source>Create a HTML widget</source>
        <translation>Créer un widget HTML</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <source>Widget name</source>
        <translation>Nom du Widget</translation>
    </message>
    <message>
        <source>Created by</source>
        <translation>Créé par</translation>
    </message>
    <message>
        <source>Tags</source>
        <translation>Tags</translation>
    </message>
</context>
<context>
    <name>ImportWebmConvert</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>AnalyseVideo...</source>
        <translation>AnalyseVidéo...</translation>
    </message>
    <message>
        <source>Generating preview image...</source>
        <translation>Génération de l&apos;image d&apos;aperçu...</translation>
    </message>
    <message>
        <source>Generating preview thumbnail image...</source>
        <translation>Génération de la miniature de l’aperçu...</translation>
    </message>
    <message>
        <source>Generating 5 second preview video...</source>
        <translation>Génération d’une vidéo d’aperçu de 5 secondes...</translation>
    </message>
    <message>
        <source>Generating preview gif...</source>
        <translation>Génération du gif d&apos;aperçu...</translation>
    </message>
    <message>
        <source>Converting Audio...</source>
        <translation>Conversion de l’audio...</translation>
    </message>
    <message>
        <source>Converting Video... This can take some time!</source>
        <translation>Conversion de la vidéo... Cela peut prendre un certain temps !</translation>
    </message>
    <message>
        <source>Converting Video ERROR!</source>
        <translation>Erreur de conversion vidéo !</translation>
    </message>
    <message>
        <source>Analyse Video ERROR!</source>
        <translation>Erreur d&apos;Analyse Vidéo !</translation>
    </message>
    <message>
        <source>Import a video to a wallpaper</source>
        <translation>Convertir une vidéo en fond d&apos;écran</translation>
    </message>
    <message>
        <source>Generating preview video...</source>
        <translation>Génération de la vidéo d&apos;aperçu...</translation>
    </message>
    <message>
        <source>Name (required!)</source>
        <translation>Nom (requis!)</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <source>Youtube URL</source>
        <translation>URL Youtube&#xa0;</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <source>Save Wallpaper...</source>
        <translation>Enregistrer le fond d&apos;écran</translation>
    </message>
</context>
<context>
    <name>ImportWebmInit</name>
    <message>
        <source>Import a .webm video</source>
        <translation>Importer une vidéo .webm</translation>
    </message>
    <message>
        <source>When importing webm we can skip the long conversion. When you get unsatisfying results with the ScreenPlay importer from &apos;ideo import and convert (all types)&apos; you can also convert via the free and open source HandBrake!</source>
        <translation>Lors de l&apos;importation de webm, vous pouvez ignorer l&apos;étape de conversion. Lorsque vous obtenez des résultats insatisfaisants avec l&apos;importateur de ScreenPlay via &apos;import et conversion de vidéo (tous types)&apos; vous pouvez convertir au format webm via le logiciel HandBrake. Il est gratuit et open source!</translation>
    </message>
    <message>
        <source>Invalid file type. Must be valid VP8 or VP9 (*.webm)!</source>
        <translation>Type de fichier invalide. Doit être un VP8 ou VP9 (*.webm)!</translation>
    </message>
    <message>
        <source>Drop a *.webm file here or use &apos;Select file&apos; below.</source>
        <translation>Déposez un fichier *.webm ici ou utilisez &apos;Sélectionner un fichier&apos; ci-dessous.</translation>
    </message>
    <message>
        <source>Open Documentation</source>
        <translation>Ouvrir la documentation</translation>
    </message>
    <message>
        <source>Select file</source>
        <translation>Sélectionner un fichier</translation>
    </message>
</context>
<context>
    <name>Importh264Convert</name>
    <message>
        <source>AnalyseVideo...</source>
        <translation>AnalyseVidéo...</translation>
    </message>
    <message>
        <source>Generating preview image...</source>
        <translation>Génération de l&apos;image d&apos;aperçu...</translation>
    </message>
    <message>
        <source>Generating preview thumbnail image...</source>
        <translation>Génération de la miniature de l’aperçu...</translation>
    </message>
    <message>
        <source>Generating 5 second preview video...</source>
        <translation>Génération d’une vidéo d’aperçu de 5 secondes...</translation>
    </message>
    <message>
        <source>Generating preview gif...</source>
        <translation>Génération du gif d&apos;aperçu...</translation>
    </message>
    <message>
        <source>Converting Audio...</source>
        <translation>Conversion de l’audio...</translation>
    </message>
    <message>
        <source>Converting Video... This can take some time!</source>
        <translation>Conversion de la vidéo... Cela peut prendre un certain temps !</translation>
    </message>
    <message>
        <source>Converting Video ERROR!</source>
        <translation>Erreur de conversion vidéo !</translation>
    </message>
    <message>
        <source>Analyse Video ERROR!</source>
        <translation>Erreur d&apos;Analyse Vidéo !</translation>
    </message>
    <message>
        <source>Import a video to a wallpaper</source>
        <translation type="unfinished">Import a video to a wallpaper</translation>
    </message>
    <message>
        <source>Generating preview video...</source>
        <translation>Génération de la vidéo d&apos;aperçu...</translation>
    </message>
    <message>
        <source>Name (required!)</source>
        <translation>Nom (requis!)</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <source>Youtube URL</source>
        <translation>URL Youtube&#xa0;</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <source>Save Wallpaper...</source>
        <translation>Enregistrer le fond d&apos;écran</translation>
    </message>
</context>
<context>
    <name>Importh264Init</name>
    <message>
        <source>Import a .mp4 video</source>
        <translation>Importer une vidéo .mp4</translation>
    </message>
    <message>
        <source>ScreenPlay V0.15 and up can play *.mp4 (also more known as h264). This can improove performance on older systems.</source>
        <translation>ScreenPlay V0.15 et plus peuvent lire des fichiers *.mp4 (également connu sous le nom de h264). Cela peut améliorer les performances sur les anciens systèmes.</translation>
    </message>
    <message>
        <source>Invalid file type. Must be valid h264 (*.mp4)!</source>
        <translation>Type de fichier invalide. Doit être un h264 (*.mp4) valide !</translation>
    </message>
    <message>
        <source>Drop a *.mp4 file here or use &apos;Select file&apos; below.</source>
        <translation>Déposez un fichier *.mp4 ici ou utilisez &apos;Sélectionner un fichier&apos; ci-dessous.</translation>
    </message>
    <message>
        <source>Open Documentation</source>
        <translation>Ouvrir la documentation</translation>
    </message>
    <message>
        <source>Select file</source>
        <translation>Sélectionner un fichier</translation>
    </message>
</context>
<context>
    <name>Installed</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Refreshing!</source>
        <translation>Actualisation !</translation>
    </message>
    <message>
        <source>Pull to refresh!</source>
        <translation>Faire glisser pour actualiser !</translation>
    </message>
    <message>
        <source>Get more Wallpaper &amp; Widgets via the Steam workshop!</source>
        <translation>Obtenez plus de fonds d&apos;écran et de widgets via le Steam Workshop !</translation>
    </message>
    <message>
        <source>Open containing folder</source>
        <translation>Ouvrir le dossier source</translation>
    </message>
    <message>
        <source>Remove Item</source>
        <translation>Supprimer l&apos;élément</translation>
    </message>
    <message>
        <source>Remove via Workshop</source>
        <translation>Retirer via le Workshop</translation>
    </message>
    <message>
        <source>Open Workshop Page</source>
        <translation>Ouvrir la page du Workshop</translation>
    </message>
    <message>
        <source>Are you sure you want to delete this item?</source>
        <translation>Êtes-vous sûr de vouloir supprimer cet élément&#x202f;?</translation>
    </message>
    <message>
        <source>Export</source>
        <translation>Exporter</translation>
    </message>
    <message>
        <source>We only support adding one item at once.</source>
        <translation>Nous ne prenons en charge que l&apos;ajout d&apos;un élément à la fois.</translation>
    </message>
    <message>
        <source>File type not supported. We only support &apos;.screenplay&apos; files.</source>
        <translation>Type de fichier non pris en charge. Nous supportons uniquement les fichiers &apos;.screenplay&apos;.</translation>
    </message>
    <message>
        <source>Import Content...</source>
        <translation type="unfinished">Import Content...</translation>
    </message>
    <message>
        <source>Export Content...</source>
        <translation type="unfinished">Export Content...</translation>
    </message>
</context>
<context>
    <name>InstalledNavigation</name>
    <message>
        <source>All</source>
        <translation type="unfinished">All</translation>
    </message>
    <message>
        <source>Scenes</source>
        <translation>Scènes</translation>
    </message>
    <message>
        <source>Videos</source>
        <translation>Vidéos</translation>
    </message>
    <message>
        <source>Widgets</source>
        <translation>Widgets</translation>
    </message>
</context>
<context>
    <name>InstalledWelcomeScreen</name>
    <message>
        <source>Get free Widgets and Wallpaper via the Steam Workshop</source>
        <translation>Obtenez des Widgets et des fonds d&apos;écran gratuits via le Steam Workshop</translation>
    </message>
    <message>
        <source>Browse the Steam Workshop</source>
        <translation>Parcourir le Steam Workshop</translation>
    </message>
    <message>
        <source>Get content via our forum</source>
        <translation>Obtenir du contenu via notre forum</translation>
    </message>
    <message>
        <source>Open the ScreenPlay forum</source>
        <translation type="unfinished">Open the ScreenPlay forum</translation>
    </message>
</context>
<context>
    <name>Monitors</name>
    <message>
        <source>Wallpaper Configuration</source>
        <translation>Configuration du fond d&apos;écran</translation>
    </message>
    <message>
        <source>Remove selected</source>
        <translation>Retirer l&apos;écran sélectionné</translation>
    </message>
    <message>
        <source>Wallpapers</source>
        <translation>Fonds d’écran</translation>
    </message>
    <message>
        <source>Widgets</source>
        <translation>Widgets</translation>
    </message>
    <message>
        <source>Remove all </source>
        <translation type="unfinished">Remove all </translation>
    </message>
</context>
<context>
    <name>MonitorsProjectSettingItem</name>
    <message>
        <source>Set color</source>
        <translation>Définir la couleur</translation>
    </message>
    <message>
        <source>Please choose a color</source>
        <translation>Veuillez choisir une couleur</translation>
    </message>
</context>
<context>
    <name>Navigation</name>
    <message>
        <source>Create</source>
        <translation>Créer</translation>
    </message>
    <message>
        <source>Workshop</source>
        <translation>Workshop</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>Installé</translation>
    </message>
    <message>
        <source>Community</source>
        <translation>Communauté</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <source>Mute/Unmute all Wallpaper</source>
        <translation>Activer/désactiver le son tous les fonds d&apos;écran</translation>
    </message>
    <message>
        <source>Pause/Play all Wallpaper</source>
        <translation>Pause/Lecture de tous les fonds d&apos;écran</translation>
    </message>
    <message>
        <source>Configure Wallpaper</source>
        <translation>Configurer le fond d&apos;écran</translation>
    </message>
    <message>
        <source>Support me on Patreon!</source>
        <translation>Soutenez-moi sur Patreon !</translation>
    </message>
    <message>
        <source>Close All Content</source>
        <translation type="unfinished">Close All Content</translation>
    </message>
</context>
<context>
    <name>QMLWallpaper</name>
    <message>
        <source>Create a QML Wallpaper</source>
        <translation>Créer un fond d&apos;écran QML</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <source>Wallpaper name</source>
        <translation>Nom du fond d&apos;écran</translation>
    </message>
    <message>
        <source>Created By</source>
        <translation>Créé par</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <source>License &amp; Tags</source>
        <translation>Licence &amp; Tags</translation>
    </message>
    <message>
        <source>Preview Image</source>
        <translation>Image d&apos;aperçu</translation>
    </message>
</context>
<context>
    <name>QMLWallpaperMain</name>
    <message>
        <source>My ScreenPlay Wallpaper 🚀</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QMLWidget</name>
    <message>
        <source>Create a QML widget</source>
        <translation>Créer un widget QML</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <source>Widget name</source>
        <translation>Nom du Widget</translation>
    </message>
    <message>
        <source>Created by</source>
        <translation>Créé par</translation>
    </message>
    <message>
        <source>Tags</source>
        <translation>Tags</translation>
    </message>
</context>
<context>
    <name>QMLWidgetMain</name>
    <message>
        <source>My Widget 🚀</source>
        <translation type="unfinished">My Widget 🚀</translation>
    </message>
</context>
<context>
    <name>SaveNotification</name>
    <message>
        <source>Profile saved successfully!</source>
        <translation>Profil enregistré avec succès !</translation>
    </message>
</context>
<context>
    <name>ScreenPlayItem</name>
    <message>
        <source>NEW</source>
        <translation>NOUVEAU</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <source>Autostart</source>
        <translation>Démarrage automatique</translation>
    </message>
    <message>
        <source>ScreenPlay will start with Windows and will setup your Desktop every time for you.</source>
        <translation>ScreenPlay démarrera avec Windows et configurera votre bureau à chaque fois pour vous.</translation>
    </message>
    <message>
        <source>High priority Autostart</source>
        <translation>Démarrage automatique haute priorité</translation>
    </message>
    <message>
        <source>This options grants ScreenPlay a higher autostart priority than other apps.</source>
        <translation>Cette option accorde à ScreenPlay une priorité de démarrage automatique plus élevée que les autres applications.</translation>
    </message>
    <message>
        <source>Send anonymous crash reports and statistics</source>
        <translation>Envoyer des rapports d&apos;incidents et des statistiques anonymes</translation>
    </message>
    <message>
        <source>Help us make ScreenPlay faster and more stable. All collected data is purely anonymous and only used for development purposes! We use &lt;a href=&quot;https://sentry.io&quot;&gt;sentry.io&lt;/a&gt; to collect and analyze this data. A &lt;b&gt;big thanks to them&lt;/b&gt; for providing us with free premium support for open source projects!</source>
        <translation>Aidez-nous à rendre ScreenPlay plus rapide et plus stable. Toutes les données collectées sont purement anonymes et utilisées à des fins de développement uniquement ! Nous utilisons &lt;a href=&quot;https://sentry.io&quot;&gt;sentry.io&lt;/a&gt; pour collecter et analyser ces données. Un &lt;b&gt;grand merci à eux&lt;/b&gt; de nous fourni un support premium gratuit pour les projets open source !</translation>
    </message>
    <message>
        <source>Set save location</source>
        <translation>Définir l&apos;emplacement de sauvegarde</translation>
    </message>
    <message>
        <source>Set location</source>
        <translation>Définir l&apos;emplacement</translation>
    </message>
    <message>
        <source>Your storage path is empty!</source>
        <translation type="unfinished">Your storage path is empty!</translation>
    </message>
    <message>
        <source>Important: Changing this directory has no effect on the workshop download path. ScreenPlay only supports having one content folder!</source>
        <translation>Important : La modification de ce répertoire n&apos;a aucun effet sur le chemin de téléchargement du workshop. ScreenPlay ne prend en charge qu&apos;un seul dossier de contenu !</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Langue</translation>
    </message>
    <message>
        <source>Set the ScreenPlay UI Language</source>
        <translation>Définir la langue de l&apos;interface de ScreenPlay</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Thème</translation>
    </message>
    <message>
        <source>Switch dark/light theme</source>
        <translation>Changer de thème sombre/clair</translation>
    </message>
    <message>
        <source>System Default</source>
        <translation>Défaut du système</translation>
    </message>
    <message>
        <source>Dark</source>
        <translation>Sombre</translation>
    </message>
    <message>
        <source>Light</source>
        <translation>Clair</translation>
    </message>
    <message>
        <source>Performance</source>
        <translation>Performance</translation>
    </message>
    <message>
        <source>Pause wallpaper video rendering while another app is in the foreground</source>
        <translation>Mettre en pause le rendu vidéo du fond d&apos;écran quand une autre application est en premier plan</translation>
    </message>
    <message>
        <source>We disable the video rendering (not the audio!) for the best performance. If you have problem you can disable this behaviour here. Wallpaper restart required!</source>
        <translation>Nous désactivons le rendu vidéo (pas l&apos;audio) pour obtenir les meilleures performances. Si vous rencontrez des problèmes, vous pouvez désactiver ce comportement ici. Redémarrage du fond d&apos;écran requis !</translation>
    </message>
    <message>
        <source>Default Fill Mode</source>
        <translation>Mode de remplissage par défaut</translation>
    </message>
    <message>
        <source>Set this property to define how the video is scaled to fit the target area.</source>
        <translation>Définissez cette propriété afin de déterminer la façon dont la vidéo est mise à l&apos;échelle pour s&apos;adapter à la zone cible.</translation>
    </message>
    <message>
        <source>Stretch</source>
        <translation>Étirer</translation>
    </message>
    <message>
        <source>Fill</source>
        <translation>Remplir</translation>
    </message>
    <message>
        <source>Contain</source>
        <translation>Contenir</translation>
    </message>
    <message>
        <source>Cover</source>
        <translation>Couvrir</translation>
    </message>
    <message>
        <source>Scale-Down</source>
        <translation>Réduire</translation>
    </message>
    <message>
        <source>About</source>
        <translation>À propos</translation>
    </message>
    <message>
        <source>Thank you for using ScreenPlay</source>
        <translation>Merci d&apos;utiliser ScreenPlay</translation>
    </message>
    <message>
        <source>Hi, I&apos;m Elias Steurer also known as Kelteseth and I&apos;m the developer of ScreenPlay. Thank you for using my software. You can follow me to receive updates about ScreenPlay here:</source>
        <translation>Salut, je suis Elias Steurer aussi connu sous le nom de Kelteseth et je suis le développeur de ScreenPlay. Merci d&apos;utiliser mon logiciel. Vous pouvez me suivre pour recevoir les mises à jour concernant ScreenPlay ici :</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <source>Open Changelog</source>
        <translation>Ouvrir le journal des modifications</translation>
    </message>
    <message>
        <source>Third Party Software</source>
        <translation>Logiciel tiers</translation>
    </message>
    <message>
        <source>ScreenPlay would not be possible without the work of others. A big thank you to: </source>
        <translation>ScreenPlay ne serait pas possible sans le travail des autres. Un grand merci à : </translation>
    </message>
    <message>
        <source>Licenses</source>
        <translation>Licences</translation>
    </message>
    <message>
        <source>Logs</source>
        <translation>Logs</translation>
    </message>
    <message>
        <source>If your ScreenPlay missbehaves this is a good way to look for answers. This shows all logs and warning during runtime.</source>
        <translation>Si ScreenPlay ne fonctionne pas correctement, c&apos;est une bonne façon de chercher des réponses. Cela montre tous les logs et les avertissements émis pendant l&apos;exécution.</translation>
    </message>
    <message>
        <source>Show Logs</source>
        <translation>Afficher les logs</translation>
    </message>
    <message>
        <source>Data Protection</source>
        <translation>Protection des données</translation>
    </message>
    <message>
        <source>We use you data very carefully to improve ScreenPlay. We do not sell or share this (anonymous) information with others!</source>
        <translation>Nous utilisons vos données exclusivement pour améliorer ScreenPlay. Nous ne vendons pas ou ne partageons pas ces données (anonyme) avec d&apos;autres !</translation>
    </message>
    <message>
        <source>Privacy</source>
        <translation>Confidentialité</translation>
    </message>
</context>
<context>
    <name>SettingsExpander</name>
    <message>
        <source>Copy text to clipboard</source>
        <translation>Copier le texte dans le presse-papiers</translation>
    </message>
</context>
<context>
    <name>Sidebar</name>
    <message>
        <source>Set Wallpaper</source>
        <translation>Définir le fond d&apos;écran</translation>
    </message>
    <message>
        <source>Set Widget</source>
        <translation>Définir le Widget</translation>
    </message>
    <message>
        <source>Headline</source>
        <translation type="unfinished">Headline</translation>
    </message>
    <message>
        <source>Select a Monitor to display the content</source>
        <translation>Sélectionnez un écran pour afficher le contenu</translation>
    </message>
    <message>
        <source>Set Volume</source>
        <translation>Régler le volume</translation>
    </message>
    <message>
        <source>Fill Mode</source>
        <translation>Mode de remplissage</translation>
    </message>
    <message>
        <source>Stretch</source>
        <translation>Étirer</translation>
    </message>
    <message>
        <source>Fill</source>
        <translation>Remplir</translation>
    </message>
    <message>
        <source>Contain</source>
        <translation>Contenir</translation>
    </message>
    <message>
        <source>Cover</source>
        <translation>Couvrir</translation>
    </message>
    <message>
        <source>Scale-Down</source>
        <translation>Réduire</translation>
    </message>
</context>
<context>
    <name>StartInfo</name>
    <message>
        <source>Free tools to help you to create wallpaper</source>
        <translation>Outils gratuits pour vous aider à créer des fonds d&apos;écran</translation>
    </message>
    <message>
        <source>Below you can find tools to create wallaper, beyond the tools that ScreenPlay provides for you!</source>
        <translation>Vous trouverez ci-dessous des outils pour créer des fonds d&apos;écran, au-delà des outils que ScreenPlay met à votre disposition !</translation>
    </message>
</context>
<context>
    <name>WebsiteWallpaper</name>
    <message>
        <source>Create a Website Wallpaper</source>
        <translation>Créer un fond d&apos;écran web</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <source>Wallpaper name</source>
        <translation>Nom du fond d&apos;écran</translation>
    </message>
    <message>
        <source>Created By</source>
        <translation>Créé par</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <source>Tags</source>
        <translation>Tags</translation>
    </message>
    <message>
        <source>Preview Image</source>
        <translation>Image d&apos;aperçu</translation>
    </message>
</context>
<context>
    <name>WizardPage</name>
    <message>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <source>Saving...</source>
        <translation>Enregistrement...</translation>
    </message>
</context>
<context>
    <name>XMLNewsfeed</name>
    <message>
        <source>News &amp; Patchnotes</source>
        <translation>Actualités et notes de mise à jour</translation>
    </message>
</context>
</TS>
